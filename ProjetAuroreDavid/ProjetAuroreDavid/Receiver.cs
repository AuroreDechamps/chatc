﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Json;
using System.Windows.Forms;
using System.Text;
using System.Threading;
using Newtonsoft.Json.Linq;


namespace ProjetAuroreDavid
{

    class Receiver
    {
        ChatControler cs;
        public Receiver()
        {
            Thread ctThread = new Thread(recu);
            ctThread.Start();
            cs = ChatControler.newInstance();



        }

        public void recu()
        {
            UdpClient listener = new UdpClient(2323);
            IPEndPoint ep = new IPEndPoint(IPAddress.Any, 2323);
            while (true)
            {

                byte[] bytes = listener.Receive(ref ep);

                String msg = Encoding.ASCII.GetString(bytes, 0, bytes.Length);
                //json parser
                dynamic data = JObject.Parse(msg);
                Console.WriteLine("COUCOU" + msg + "######" + data.type);
                //si type hello
                if (data.type == "HELLO_R" || data.type == "HELLO_A")
                {
                    //deserialization du hello
                   

                    HELLO deserializedHello = new HELLO();
                    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(msg));
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedHello.GetType());
                    deserializedHello = ser.ReadObject(ms) as HELLO;
                    ms.Close();
                    Console.WriteLine(deserializedHello.Addr_source);
                    //appel de la fonction helloReceive


                    if (data.type == "HELLO_R")
                    {
                        cs.helloRReceive(deserializedHello);
                        //cs.MajListPseudo(deserializedHello.Addr_source);
                      
                    }
                    else cs.helloAReceive(deserializedHello);
                }
                else if (data.type == "MESSAGE")
                {
                    
                    Message deserializedMessage = new Message();
                    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(msg));
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedMessage.GetType());
                    deserializedMessage = ser.ReadObject(ms) as Message;
                    ms.Close();
                    Console.WriteLine(deserializedMessage.Msg);
                   
                    Console.WriteLine("LE HASH DU MESSAGE" + deserializedMessage.Hash);
                    cs.myDelegate.Invoke(deserializedMessage);
                }

                else if(data.type == "GOODBYE") {
                    GOODBYE deserializedGoodBye = new GOODBYE();
                    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(msg));
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedGoodBye.GetType());
                    deserializedGoodBye = ser.ReadObject(ms) as GOODBYE;
                    ms.Close();
                    Console.WriteLine("GOODBYE " + msg);
                    cs.deletePair(deserializedGoodBye);
                }

                //si on reçoit un pong alors suppression du pair dans la liste de pong a recevoir contenu dans le controller
                else if (data.type == "PONG")
                {
                    Console.WriteLine("J'ai reçu un pong");
                    PINGPONG deserializedPong = new PINGPONG();
                    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(msg));
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedPong.GetType());
                    deserializedPong = ser.ReadObject(ms) as PINGPONG;
                    ms.Close();
                    //parcours liste noeud
                    foreach (Pair p in cs.Noeuds) {
                        //si adresse pong recu = a adresse d'un pair
                        if(p.Addr == deserializedPong.Addr_source) {
                            //si on attend le pong de ce pair
                            if(cs.PingWaitPong.Contains(p)) {
                                //supression de la liste des pong attendu
                                cs.PingWaitPong.Remove(p);
                            }
                        }
                    }
                      
                }

                //si on reçoit un ping, on envoie un pong
                else if (data.type == "PING")
                {
                    Console.WriteLine("J'ai reçu un ping");
                    PINGPONG deserializedPing = new PINGPONG();
                    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(msg));
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedPing.GetType());
                    deserializedPing = ser.ReadObject(ms) as PINGPONG;
                    ms.Close();
                    cs.sendPong(deserializedPing);
                }



            }
        }


    }
}
