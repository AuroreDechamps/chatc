﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using TP1MBDS;

namespace ProjetAuroreDavid
{
    [DataContract]
    class Listener
    {


        internal Message Btn_Click(Object sender, EventArgs e, System.Windows.Controls.TextBox t)
        {
            ChatControler cc = ChatControler.newInstance();
            var a = ((System.Windows.Controls.Button)sender).DataContext as Listener;
            //message
            Message message = recupeValeur(t);
            //notre nickname
            message.Nickname = cc.Nickname;
            //ajout du nickname dans le rootedBy
            message.Rootedby1 = cc.Nickname;
            //ajout du hash dans la liste hash du msg  afin de ne pas l'envoyer 2 fois
            cc.MsgList.Add(message.Hash);
            Console.WriteLine("< " + message.Nickname + " > : " + message.Msg + " " + message.Timestamp);
            TextBox txt;
            txt = t.FindName("Input") as TextBox;
            txt.Clear();
            ChatControler chat = ChatControler.newInstance();
            //envoie du message
            cc.sendObject(message);

            ChatRoom ch = cc.getChatRoom(message.Destinataire);
            ch.AddMessage(message);


            return message;


        }





        internal Message recupeValeur(System.Windows.Controls.TextBox t)
        {
            string msg = t.Text.ToString();
            string localIP;
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                localIP = endPoint.Address.ToString();
            }
            Console.WriteLine(localIP + "  message envoyé : " + msg);

            Message message = new Message(localIP, msg, " à tous le monde");
            return message;
        }


    }
}
