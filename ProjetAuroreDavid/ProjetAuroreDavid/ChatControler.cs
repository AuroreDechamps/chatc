﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using TP1MBDS;


namespace ProjetAuroreDavid
{
    internal delegate void MessageReceive(Message message);
    internal delegate void HelloReceive(HELLO h);
    class ChatControler
    {
        //10.154.127.244
        public MessageReceive myDelegate;
        public HelloReceive myDelegateHello;
        ObservableCollection<Pair> noeuds;
        ObservableCollection<Pair> pingWaitPong;
        ObservableCollection<ChatRoom> chat_rooms;
        private static ChatControler singleton;
        private string localIP;
        private string helloTo;
        private int port;
        private int helloToIndex;
        private Label messageLabel;
        private Grid essageZone;
        private string nickname;
        private Label pseudoList = new Label();
        private Grid pseudoZone = new Grid();

        public Label PseudoList { get; set; }
        public Grid PseudoZone { get; set; }

        private List<string> msgList;

        public List<string> contactList;

        internal ObservableCollection<Pair> Noeuds
        {
            get
            {
                return noeuds;
            }

            set
            {
                noeuds = value;
            }
        }

        internal ObservableCollection<Pair> PingWaitPong
        {
            get
            {

                return pingWaitPong;
            }

            set
            {
                pingWaitPong = value;
            }
        }

        internal ObservableCollection<ChatRoom> Chat_rooms
        {
            get
            {
                return chat_rooms;
            }

            set
            {
                chat_rooms = value;
            }
        }

        public string LocalIP
        {
            get
            {
                return localIP;
            }

            set
            {
                localIP = value;
            }
        }

        public Label MessageLabel
        {
            get
            {
                return messageLabel;
            }

            set
            {
                messageLabel = value;
            }
        }

        public Grid EssageZone
        {
            get
            {
                return essageZone;
            }

            set
            {
                essageZone = value;
            }
        }

        public string Nickname
        {
            get
            {
                return nickname;
            }

            set
            {
                nickname = value;
            }
        }

        public List<string> MsgList
        {
            get
            {
                return msgList;
            }

            set
            {
                msgList = value;
            }
        }

        /// CONSTRUCTEUR //////////////////////////////////////////////////////////////////////////////
        private ChatControler()
        {
            this.port = 2323;
            //delegue qui est appele par le thread
            myDelegate = new MessageReceive(MessageRecu);
            myDelegateHello = new HelloReceive(helloRReceive);

            //nouveau noeud (liste des pairs)
            Noeuds = new ObservableCollection<Pair>();

            pingWaitPong = new ObservableCollection<Pair>();
            //nouvelle liste de chatRoom
            Chat_rooms = new ObservableCollection<ChatRoom>();
            //socket
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                this.localIP = endPoint.Address.ToString();
            }
            //ajout du pair dans le noeud
        //  Pair p = new Pair("10.154.127.244", "2323");
             Pair p = new Pair("169.254.69.64", "2323");
        //    Pair p = new Pair("10.154.124.248", "2323");
           
            Noeuds.Add(p);
            //nom de l'utilisateur
            nickname = "David";

            msgList = new List<string>();
            contactList = new List<string>();

            System.Timers.Timer myTymer = new System.Timers.Timer();
            myTymer.Elapsed += new System.Timers.ElapsedEventHandler(sendPing);
            myTymer.Interval = 10000;
            myTymer.Enabled = true;
        }





        //methode du delegue lorsqu'on reçoit un message
        public void MessageRecu(Message message)
        {

        //recuperer les pseudos afin d'avoir une liste de gens a qui nous pourrons envoyer des messages perso
            Console.WriteLine("avant premier if" + message.Hash);
            //si msg pas deja traite 
            if (!msgList.Contains(message.Hash))
            {
                Console.WriteLine("avant deuxieme if");
                //si message destine a tt le monde ou si destine a moi alors affichage
                if (message.Destinataire.Length ==0 )
                {
                    Console.WriteLine("apres deuxieme if");
                    //gere les thread, fonction lambda
                    MessageLabel.Dispatcher.Invoke(() =>
                    {
                        //////////RECUPERATION DE LA CHATROOOM ET AJOUT DU MESSAGE SI ON AFFICHE LE MESSAGE
                        ChatRoom ch = getChatRoom(message.Destinataire);
                        ch.AddMessage(message);
                        //le thread principal appel incomingMessage qui affiche le message 
                        incomingMessage(message.Nickname, message.Msg);
                    });
                }
                //on affiche le message si le nickname ne commence pas par @ ou s'il nous est destine
                else if (!message.Destinataire[0].Equals('@') || message.Destinataire.Equals("@" + nickname))
                {
                    //gere les thread, fonction lambda
                    MessageLabel.Dispatcher.Invoke(() =>
                    {


                        //////////RECUPERATION DE LA CHATROOOM ET AJOUT DU MESSAGE SI ON AFFICHE LE MESSAGE
                        ChatRoom ch = getChatRoom(message.Destinataire);
                        //si message destine a nous alors on affiche la chatRoom de l'envoyeur, le nickname
                        if (message.Destinataire.Equals("@" + nickname)) {
                            ch = getChatRoom(message.Nickname);
                        }
                       
                        ch.AddMessage(message);
                       

                        //le thread principal appel incomingMessage qui affiche le message 
                        incomingMessage(message.Nickname, message.Msg);
                        //Ajout a la liste de contact si pas déjà fait 
                        if (!contactList.Contains(message.Destinataire)) {
                            contactList.Add(message.Destinataire);
                        }  
                    });
                }
                //si message pas uniquement pour nous 

                msgList.Add(message.Hash);
                //ajout au rootedBy de notre nickname
                message.Rootedby1 += "," + this.Nickname;
                //separation, tableau de string pour recuperer chaque nickname
                string[] rooteby = message.Rootedby1.Split(',');
                //parcours des nickname
                foreach(string contactNickname in rooteby) {
                    //si pas déjà dans notre liste de contact alors ajout
                    if (!contactList.Contains(contactNickname))
                    {
                        contactList.Add(contactNickname);
                    }
                }
                if (!message.Destinataire.Equals("@" + nickname))
                {
                    sendObject(message);
                }

            }

        }

        //gestion du singleton 
        public static ChatControler newInstance()
        {
            if (singleton == null)
            {
                singleton = new ChatControler();
            }
            return singleton;
        }

        //affichage du message
        public void incomingMessage(string envoyeur, String msg)
        {



            string message = "< " + envoyeur + " > : " + msg;
            MessageLabel.Content = message;
            Label e = new Label();
            e.Content = message;
            int rowCount = EssageZone.RowDefinitions.Count();
            var rowDefinition = new RowDefinition();
            EssageZone.RowDefinitions.Add(rowDefinition);
            if (rowCount == 1)
            {
                Grid.SetRow(e, rowCount);
            }
            else
            {
                Grid.SetRow(e, rowCount);
                EssageZone.Children.Add(e);
            }

        }


        //on envoie hello a notre premier pair
        public void startup()
        {
            //creation de hello
            HELLO hello = new HELLO("HELLO_A", localIP, port.ToString(), this.noeuds.ToList());
            //sauvegarde du noeud a qui on a envoyé 
            helloTo = this.noeuds[0].Addr;
            helloToIndex = 0;
            //serialisation
            StreamReader sr = serialisation(hello);
            //socket
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse(this.noeuds[0].Addr), Int32.Parse(this.noeuds[0].Port));
            byte[] msg = Encoding.ASCII.GetBytes(sr.ReadToEnd());
            s.SendTo(msg, ep);


        }

        //on se charge de la serialisation des objets ici
        public StreamReader serialisation(Object o)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(o.GetType());
            MemoryStream stream1 = new MemoryStream();
            ser.WriteObject(stream1, o);
            stream1.Position = 0;
            return new StreamReader(stream1);
        }


        //lorsqu'on reçoit un hello qui repond a notre requete (appel par le thread receiver)
        public void helloRReceive(HELLO h)
        {


            //parcours des pairs du hello reçu
            foreach (Pair p in h.Pairs)
            {
                //si on a deja le noeud, si on en a deja 4, si le pair n'est pas nous meme
                if (!noeuds.Contains(p) && noeuds.Count < 4 && p.Addr != localIP)
                {
                    //ajout a notre liste de pairs
                    Console.WriteLine("AJOUT DANS LA LISTE");
                    noeuds.Add(p);

                }
            }
            Console.WriteLine(noeuds.Count + "           HELLO R RECU  " + noeuds.ToString());
            //si on a pas encore 4 pairs
            if (noeuds.Count < 4)
            {
                helloToIndex += 1;
                //si on a bien un pair a cet index (donc taille noeuds >= index demande)
                if (noeuds.Count >= helloToIndex + 1)
                {
                    //nouveau hello a 
                    HELLO hello = new HELLO("HELLO_A", localIP, port.ToString(), this.noeuds.ToList());
                    //recuperation adresse ip au pair a qui on doit envoyer le hello
                    helloTo = this.noeuds[helloToIndex].Addr;
                    //envoie du hello
                    StreamReader sr = serialisation(hello);
                    Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    IPEndPoint ep = new IPEndPoint(IPAddress.Parse(helloTo), Int32.Parse(this.noeuds[helloToIndex].Port));
                    byte[] msg = Encoding.ASCII.GetBytes(sr.ReadToEnd());
                    s.SendTo(msg, ep);
                    //gere les thread, fonction lambda
                    PseudoList.Dispatcher.Invoke(() =>
                    {
                        //le thread principal appel incomingMessage qui affiche le message 

                        MajListPseudo(h.Addr_source);
                    });
                }
            }
        }



        //Modifie l'affichage de liste des speudo
        public void MajListPseudo(String message)
        {
            string msg = message;
            PseudoList.Content = msg;
            Label e = new Label();
            e.Content = message;
            pseudoList.Content = "texte";
            pseudoList.Content = e.Content.ToString();
            int rowCount = pseudoZone.RowDefinitions.Count();
            var rowDefinition = new RowDefinition();
            pseudoZone.RowDefinitions.Add(rowDefinition);
            if (rowCount == 1)
            {
                Grid.SetRow(e, rowCount);
            }
            else
            {
                Grid.SetRow(e, rowCount);
                pseudoZone.Children.Add(e);
            }
        }

        //lorsqu'on nous demande une reponse
        public void helloAReceive(HELLO h)
        {

            //si on a pas plus de 4 noeuds ou si le hello n'a pas de pair

            //gere les thread, fonction lambda
            PseudoList.Dispatcher.Invoke(() =>
            {
                //le thread principal appel incomingMessage qui affiche le message 

                MajListPseudo(h.Addr_source);
            });
            //on rajoute a nos pairs si pas deja fait
            Pair p = new Pair(h.Addr_source, h.Port_source);
            Console.WriteLine("HELLO A "+ p.Addr,p.Port);
            Boolean alreadyPair = false;
            foreach(Pair pa in noeuds) {
                if(p.Addr == h.Addr_source) {
                    alreadyPair = true;
                }
            }

            if (!alreadyPair && this.noeuds.Count < 4)
            {
                this.noeuds.Add(p);

            }
            //on repond au hello
            HELLO hello = new HELLO("HELLO_R", localIP, port.ToString(), this.noeuds.ToList());

            //serialisation su hello
            StreamReader sr = serialisation(hello);
            //socket et envoie
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse(h.Addr_source), Int32.Parse(h.Port_source));
            byte[] msg = Encoding.ASCII.GetBytes(sr.ReadToEnd());
            s.SendTo(msg, ep);


        }

        //on envoie goodbye lorsqu'on quitte l'application
        public void goodbye()
        {
            GOODBYE g = new GOODBYE(this.localIP, nickname);
            sendObject(g);
        }





        //lorsqu'on a reçu un ping, on envoie un pong
        public void sendPong(PINGPONG p) {
            Console.WriteLine("J'ENVOIE UN PONG");
            //nouveau pong
            PINGPONG pong = new PINGPONG("PONG", localIP);   
            //serialisation du PONG
            StreamReader sr = serialisation(pong);
            //socket et envoie
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //on recupere le port du pair nous ayant envoye le ping
            int portPong = this.port;
            //parcours des pairs
            foreach(Pair pa in noeuds) {
                //si mm adresse ip que adresse ip du ping
                if (pa.Addr == p.Addr_source) {
                    //recuperation du port du pair a qui nous devons envoyer le pong
                    portPong = Int32.Parse(pa.Port);
                }
            }
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse(p.Addr_source),portPong);
            byte[] msg = Encoding.ASCII.GetBytes(sr.ReadToEnd());
            s.SendTo(msg, ep);
        }


        //envoyer ping toute les 10 secondes 
        public void sendPing(object source, System.Timers.ElapsedEventArgs e) {
            //si liste des pong attendu different de null alors parcours et supression des pair de notre liste des pairs
            if(this.pingWaitPong.Count > 0) {
                foreach(Pair p in pingWaitPong) {
                    noeuds.Remove(p);
                }
            }
            
            //list pingPong remise a null
            pingWaitPong = new ObservableCollection<Pair>();

            //parcours liste pair pour envoie des ping
            foreach (Pair p in noeuds) {
                Console.WriteLine("J'ENVOIE UN PING" + noeuds.Count);
                //ajout du pair dans la liste des pong attendu
                pingWaitPong.Add(p);
                //nouveau ping
                PINGPONG ping = new PINGPONG("PING", localIP);
                //envoie du ping
                StreamReader sr = serialisation(ping);
                Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                IPEndPoint ep = new IPEndPoint(IPAddress.Parse(p.Addr), Int32.Parse(p.Port));
                byte[] msg = Encoding.ASCII.GetBytes(sr.ReadToEnd());
                s.SendTo(msg, ep);
            }

        }



        //lorsqu'on reçoit un goodbye on supprime le pair qui est parti
        public void deletePair(GOODBYE g)
        {
            Pair sauv = null;
            foreach (Pair p in noeuds)
            {
                if (p.Addr == g.Addr_source)
                {
                    sauv = p;
                }
            }
            if (sauv != null) noeuds.Remove(sauv);

            Console.WriteLine(noeuds.Count);
        }

        //encoyer objet a tt les pairs
        public void sendObject(object o)
        {
            StreamReader sr = serialisation(o);
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            foreach (Pair p in noeuds)
            {
                IPEndPoint ep = new IPEndPoint(IPAddress.Parse(p.Addr), Int32.Parse(p.Port));
                byte[] msg = Encoding.ASCII.GetBytes(sr.ReadToEnd());
                s.SendTo(msg, ep);
            }
        }

        //recuperation de la chatRoom
        public ChatRoom getChatRoom(string nom) {
            Boolean create = true;
            ChatRoom resChatRoom = null;
            //parcours liste chatRoom
           
            foreach (ChatRoom chat in chat_rooms) {
                //si chat room avec le nom deja ouverte
                if (chat.Nom == nom) {
                    //boolean a false et chatroom qui sera retourné est selectionne
                    create = false;
                    resChatRoom =  chat;
                }
            }
            //si chatRoom pas encore cree
            if(create) {
                //creation de la chatRoom et ajout dans la liste
                resChatRoom = new ChatRoom(nom, "conversation n : " + chat_rooms.Count);
                chat_rooms.Add(resChatRoom);
            }
            //on retourne le resultat (chatRoom trouve ou bien cree)
            return resChatRoom;
        }
    }
}
