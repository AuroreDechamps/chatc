﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProjetAuroreDavid
{
    [DataContract]
    class PINGPONG
    {
        [DataMember]
        private string type;
        [DataMember]
        private string addr_source;
        [DataMember]
        private string timestamp;

        public string Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public string Addr_source
        {
            get
            {
                return addr_source;
            }

            set
            {
                addr_source = value;
            }
        }

        public string Timestamp
        {
            get
            {
                return timestamp;
            }

            set
            {
                timestamp = value;
            }
        }


        public PINGPONG() {

        }

        public PINGPONG(string type, string addr_source) {
            this.Type = type;
            this.Addr_source = addr_source;
            this.Timestamp = GetTimestamp(DateTime.Now);
        }

        //retourner la date au format voulu
        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("HHmmss");
        }
    }
}
