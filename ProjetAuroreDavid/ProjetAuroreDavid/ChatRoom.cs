﻿using System;
using System.Collections.Generic;

namespace ProjetAuroreDavid
{
    class ChatRoom
    {
        private string nom;
        private string textPresentation;
        List<Message> messages;
        List<Char> voyelles = new List<char> {'a','e','i','o','u'};

        //constructeur
        public ChatRoom(string n, string tP){
            nom = n;
            textPresentation = tP;
            messages = new List<Message>();
        }

        //ajout d'un message dans la chatRoom
        public void AddMessage(Message m)
        {
            //envoye a true pour le message
           
            messages.Add(m);
        }

        //methode permettant d'afficher tous les messages
        public void display()
        {
            Console.WriteLine(nom);
            Console.WriteLine(textPresentation);
            Console.WriteLine("//////////////////////////////////////");
            //on boucle sur les messages 
            for (int i = 0; i < messages.Count; i++)
            {
                Console.WriteLine(messages[i].Nickname + " : " + messages[i].Msg);
            }
            Console.WriteLine("//////////////////////////////////////");
        }
        
        
        public string Nom
        {
            get
            {
                return nom;
            }
            set
            {
                nom = value;
            }
        }

        public string TextPresentation
        {
            get
            {
                return textPresentation;
            }

            set
            {
                textPresentation = value;
            }
        }

        public List<Message> Messages
        {
            get
            {
                return messages;
            }

            set
            {
                messages = value;
            }
        }

        public List<Char> Voyelles
        {
            get
            {
                return voyelles;
            }

            set
            {
                voyelles = value;
            }
        }

    }
}
