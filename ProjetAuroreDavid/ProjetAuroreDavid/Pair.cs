﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProjetAuroreDavid
{
    [DataContract]
    class Pair
    {
        //champs
        [DataMember]
        private string addr;
        [DataMember]
        private string port;

        //constructeur
        public Pair(string a, string p)
        {
            addr = a;
            port = p;

        }
        //accesseurs
   
        public string Port
        {
            get
            {
                return port;
            }

            set
            {
                port = value;
            }
        }

  
        public string Addr
        {
            get
            {
                return addr;
            }

            set
            {
                addr = value;
            }
        }
    }
}