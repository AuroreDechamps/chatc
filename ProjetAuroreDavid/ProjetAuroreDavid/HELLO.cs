﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProjetAuroreDavid
{
    [DataContract]
    class HELLO
    {
        [DataMember]
        private string type;
        [DataMember]
        private string addr_source;
        [DataMember]
        private string port_source;
        [DataMember]
        private List<Pair> pairs;

        public HELLO() {

        }

        public HELLO(string t ,string addr_source, string port_source, List<Pair> pair) {
            type = t;
            this.addr_source = addr_source;
            this.port_source = port_source;
            this.pairs = pair;
           
        }

        public string Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        
        public string Addr_source
        {
            get
            {
                return addr_source;
            }

            set
            {
                addr_source = value;
            }
        }


        public string Port_source
        {
            get
            {
                return port_source;
            }

            set
            {
                port_source = value;
            }
        }

  
        internal List<Pair> Pairs
        {
            get
            {
                return pairs;
            }

            set
            {
                pairs = value;
            }
        }
    }
}
