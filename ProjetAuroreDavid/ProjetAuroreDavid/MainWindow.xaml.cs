﻿using System;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using TP1MBDS;

namespace ProjetAuroreDavid
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window

    {
        static Listener li = new Listener();
        ChatControler cC = ChatControler.newInstance();
        private Timer aTimer;
        Message m;


        public MainWindow()
        {
            
            InitializeComponent();
            {

            }
            Receiver r = new Receiver();

            aTimer = new Timer();

            // Hook up the Elapsed event for the timer.
            aTimer = new System.Timers.Timer();
            // Tell the timer what to do when it elapses
            aTimer.Elapsed += new ElapsedEventHandler(myEvent);
            // Set it to go off every five seconds
            aTimer.Interval = 5000;
            // And start it        
            aTimer.Enabled = true;
            Label pseudoLabel = (Label)this.FindName("Moi");
            cC.PseudoList = pseudoLabel;
            Grid zonePeudo = (Grid)this.FindName("istePseudo");
            cC.PseudoZone = zonePeudo;
            cC.startup();
            //ma
            Label messageLabel = (Label)this.FindName("Messages");
            cC.MessageLabel = messageLabel;

            Grid messageZone = (Grid)this.FindName("messageZone");
            cC.EssageZone = messageZone;

        }

        private void myEvent(object sender, ElapsedEventArgs e)
        {
               
        }

        public void Btn_Click(Object sender, EventArgs e)
        {
            TextBox t = (TextBox)this.FindName("Input");
           
            Label messageLabel = (Label)this.FindName("Messages");
            Label envoyeurLabel = (Label)this.FindName("Envoyeur");
          
           
            Grid messageZone = (Grid)this.FindName("messageZone");
           
            Message m=li.Btn_Click(sender, e, t);

            cC.incomingMessage(cC.Nickname, m.Msg);
           

        }

        //a la fermeture de fenetre
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            //do my stuff before closing
            cC.goodbye();
            base.OnClosing(e);
        }




    }
}
