﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1MBDS
{
    //type generique, lors de l'instanciation, on doit preciser les types de k et v
    class Dico <K, V>
    {
        //deux listes
        private List<K> key;
        private List<V> value;

        //constructeur, instancie les deux listes
        public Dico()
        {
            key = new List<K>();
            value = new List<V>();
        }

        //methode ajouter, key value
        public bool add(K k, V v)
        {
            //si la clé est deja existante return false, une clé doit etre unique
            if (key.Contains(k))
            {
                return false;
            }
            //si non ajout de la clé dans la liste clé et la value dans la liste value, return true
            else
            {
                key.Add(k);
                value.Add(v);
                return true;
            }
        }

        //methode suppression avec cle passe en parametre
        public bool remove(K k)
        {
            //si clé non existante return false
            if (!key.Contains(k))
            {
                return false;
            }

            //si non suppression et return true
            else
            {
                //recuperation index de la clé (mm index que la valeur associe)
                int ind = key.LastIndexOf(k);
                //suppression de la cle
                key.Remove(k);
                //suppression de la valeur par l'index qui est au mm index que la clé supprimé
                value.RemoveAt(ind);
                return true;
            }
        }

        //affichage cle valeur
        public void display()
        {
            for (int i = 0;i< key.Count(); i++)
            {
                Console.WriteLine(key[i] + "    " + value[i]);
            }
        }

        public List<K> Key
        {
            get
            {
                return key;
            }

            set
            {
                key = value;
            }
        }

        public List<V> Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;
            }
        }
    }
}
