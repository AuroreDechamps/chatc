﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Runtime.Serialization;
using System.Xml;

namespace ProjetAuroreDavid
{
   
    [DataContract]
    class Message 
    {
        [DataMember]
        private string type;
        [DataMember]
        private string nickname;
        [DataMember]
        private string msg;
        [DataMember]
        private string timestamp;
        [DataMember]
        private string destinataire;
        [DataMember]
        private string Rootedby;
       
  
        [DataMember]
        private string hash; 

        public Message() {

        }

        public Message(string n, string t,string d)
        {
            type = "MESSAGE";
            nickname = n;
            msg = t;
            destinataire = d;
            Console.WriteLine(d);
            //recuperer la date au format definit dans la methode plus bas
            timestamp = DateTime.Now.ToString();
           
            SHA256 hashe = new SHA256Managed();
            byte[] b  =hashe.ComputeHash(Encoding.UTF8.GetBytes(type+nickname+msg+timestamp+destinataire));
            StringBuilder res = new StringBuilder();

            for (int i = 0; i < b.Length; i++)
            {
                res.Append(String.Format("{0:X2}",b[i]));
            }

            this.Hash = res.ToString();
            Console.WriteLine("test"+this.Hash);
        }

        //ajout attribut sauvegarde
    
       
        public string Nickname
        {
            get
            {
                return nickname;
            }

            set
            {
                nickname = value;
            }
        }


       
        public string Msg
        {
            get
            {
                return msg;
            }

            set
            {
                msg = value;
            }
        }
        
        public string Timestamp
        {
            get
            {
                return timestamp;
            }

            set
            {
                timestamp = value;
            }
        }
      
     
      
        public string Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }
      
        public string Destinataire
        {
            get
            {
                return destinataire;
            }

            set
            {
                destinataire = value;
            }
        }
      
        public string Rootedby1
        {
            get
            {
                return Rootedby;
            }

            set
            {
                Rootedby = value;
            }
        }

        public string Hash
        {
            get
            {
                return hash;
            }

            set
            {
                hash = value;
            }
        }




        //retourner la date au format voulu
        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("dd:MM:yyyy:HH:mm:ss");
        }

    
    }
}
