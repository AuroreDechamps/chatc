﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProjetAuroreDavid
{
    [DataContract]
    class GOODBYE
    {
        [DataMember]
        private string type;
        [DataMember]
        private string addr_source;
        [DataMember]
        private string nickname;

        public GOODBYE() {

        }

        public GOODBYE(string addr, string n) {
            type = "GOODBYE";
            this.addr_source = addr;
            Nickname = n;
        }

        public string Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public string Addr_source
        {
            get
            {
                return addr_source;
            }

            set
            {
                addr_source = value;
            }
        }

        public string Nickname
        {
            get
            {
                return nickname;
            }

            set
            {
                nickname = value;
            }
        }
    }
}
